import React, { Fragment, Component } from 'react';
import './App.css';
import Member from '../Member/Member';
// Fragment allow us to return several stuff
// Without fragment, I should put the <div> <h2> inside another <div> global

 // 
const familyCollection = {
  member1: {
    name :'Gabriel',
    age: 3
  },
  member2: {
    name :'Alexander',
    age: 18
  },
  member3: {
    name :'Maria',
    age: 25
  },
  member4: {
    name :'Alexa',
    age: 50
  }
}




class App extends Component {
  state = {
    familyCollection 
  }
  handeClick = () => {
    const famille = { ...this.state.familyCollection}
    famille.member1.age += 1;
    this.setState({famille});
  }

  render(){
  const { familyCollection } = this.state 
    return (
      <Fragment>
          <div className="App">
            <h1>Application 1</h1>
            <Member 
              name= {familyCollection.member1.name} 
              age =  {familyCollection.member1.age}/>
            <Member 
              name = {familyCollection.member2.name}
              age = {familyCollection.member2.age}/>
            <Member 
              name = {familyCollection.member3.name}
              age = {familyCollection.member3.age}/>
            <Member 
              name = {familyCollection.member4.name}
              age = {familyCollection.member4.age}>
              <strong>I am Alexa's prop.child</strong>
            </Member>
            <button onClick = 
            {this.handeClick}
            >
              Getting older
            </button>
          </div>
        

          <h2>Title that make the Fragment usefull</h2> 
      </Fragment>
    )
  }
}
export default App;



// SETTING STATE WITH USESTATE & HOOKS
  /*
  const [myFamily, setFamily]  = useState({
    member1: {
    name :'Gabriel',
    age: 3
    },
    member2: {
      name :'Alexander',
      age: 18
    },
    member3: {
      name :'Maria',
      age: 25
    },
    member4: {
      name :'Alexa',
      age: 50
    }
  },)
   
  
    const [state, dispatch] = useReducer(
    reducer,
    {familyCollection: familyCollection },
  );
  */

/*
THIS IS JSX CODE  =>
<div className="App">
      <h1>Application 1</h1>
</div>

WILL BE CONVERTED INTO JAVASCRIPT (see below)
THEN JAVASCRIPT CONVERTED INTO HTML


THIS IS JAVASCRIPT CODE =>
React.createElement('div', {className :'App'}, React.createElement('h1', null, 'Application 1'))
 * 
 * 
 */

