import React, { Fragment, useState } from 'react';
import './App.css';
import Member from '../Member/Member';
import Button from '../Button/Button'
// Fragment allow us to return several stuff
// Without fragment, I should put the <div> <h2> inside another <div> global



function App() {
  // SETTING STATE WITH USESTATE & HOOKS 
  const [myFamily, setFamily]  = useState({
    member1: {
    name :'Gabriel',
    age: 3
    },
    member2: {
      name :'Alexander',
      age: 18
    },
    member3: {
      name :'Maria',
      age: 25
    },
    member4: {
      name :'Alexa',
      age: 50
    }
  },)
  const [count, setCount] = useState(0);
  const [isShow, setIsShow] = useState(false);

  const handleShowDetails = function(){
    let show = !isShow;
    console.log(show);
    setIsShow(show);
  }
  const handleClick = function(num){
    const newFamily = { ...myFamily}
    newFamily.member1.age += num;
    setFamily(newFamily);
  } 
  const handleChange = function(event, id){
    const newFamily = { ...myFamily}
    const newName = event.target.value;
    newFamily[id].name = newName;
    setFamily(newFamily);
  } 
  const hideMember = function(id){
    const newFamily = { ...myFamily}
    newFamily[id].name = 'X'
    setFamily(newFamily);
  } 

  let description = null;
  if(isShow){
    description = <strong>I am Alexa's prop.child</strong>  
  }

  const liste = Object.keys(myFamily)
      .map(membre => (
    <Member 
      key = {membre}
      handleChange = {event => handleChange(event, membre)}
      hideMember={() => hideMember(membre)}
      name= {myFamily[membre].name} 
      age =  {myFamily[membre].age}/>
  ))
  console.log(liste);
  
 
  return (
    <Fragment>
        <div className="App">
          <h1>Application 1</h1>
       
          {liste}
         {/*  <Member 
            name = {myFamily.member4.name}
            age = {myFamily.member4.age}>
            {description}
            <button onClick={handleShowDetails}>
              {
               isShow ? 'Cacher' : 'Montrer'
              }
            </button>
            </Member>*/}
          
          
          <button onClick = {() => handleClick(1)}>
              Getting older
            </button>
             
         <Button vieillir= {() => handleClick(2)}></Button>
          <button onClick={() => setCount(count + 1)}> 
            Click me  {count}
          </button>
         
        </div>
       

        <h2>Title that make the Fragment usefull</h2> 
    </Fragment>
  )
}


/*
THIS IS JSX CODE  =>
<div className="App">
      <h1>Application 1</h1>
</div>

WILL BE CONVERTED INTO JAVASCRIPT (see below)
THEN JAVASCRIPT CONVERTED INTO HTML


THIS IS JAVASCRIPT CODE =>
React.createElement('div', {className :'App'}, React.createElement('h1', null, 'Application 1'))
 */


 /* ----------- GARBAGE ----------------

 ----------- reducer -----------------
 function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {familyCollection: state.familyCollection.member1.age + 1};
    default:
      throw new Error();
  }
}
 const [state, dispatch] = useReducer(
    reducer,
    {familyCollection: familyCollection },
  );


--------------- state -------------
const familyCollection = {
  member1: {
    name :'Gabriel',
    age: 3
  },
  member2: {
    name :'Alexander',
    age: 18
  },
  member3: {
    name :'Maria',
    age: 25
  },
  member4: {
    name :'Alexa',
    age: 50
  }
}
 */
export default App;
