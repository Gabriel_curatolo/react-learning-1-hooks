import React from 'react'

function Button({vieillir}){
    return (
        <button onClick={vieillir}>
            Getting older of 2 years using button component
        </button>
    )
}

export default Button