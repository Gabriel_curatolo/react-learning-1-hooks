import React, { Fragment } from 'react'

// Using a function instead of a class component makes it simpler to read for the programmer
// Without a state 
function Member({name, age, children, hideMember, handleChange}){
    // props is an object, by doing props.name I get the name of the value
    return(
        <Fragment>
            
            <h2>{name.toUpperCase()} -  {age} years</h2>
            
            <input value={name} onChange={handleChange} type='text' /> 
            <button onClick={hideMember}>X</button>
            { children ? <p>{children}</p> : <br/> }
           
        </Fragment>

       
    )
}
// If there is a children, we do <p> {children} </p>, else empty fragment
//{ children ? <p>{children}</p> : <Fragment/> }



export default Member



/* Way number 1 of using props 
function Member(props){
    // props is an object, by doing props.name I get the name of the value
    let name = props.name

    return(
        <h2>Member of my family : {name.toUpperCase()}</h2>
    )
}
*/